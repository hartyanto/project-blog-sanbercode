
## About Laravelloka

Untuk bisa menggunakan :

- Lakukan clone dengan menggunakan ssh atau https
- Setelah proses clone selesai, jalankan composer install
- Buat file .env dan lakukan spesifikasi database
- Tambahkan FILESYSTEM_DRIVER=public pada file .env
- Jalankan perintah php artisan key:generate
- Jalankan perintah php artisan storage:link untuk bisa menggunakan file upload di post
- Lakukan migrasi dan seeding dengan php artisan migrate dan php artisan db:seed
- php artisan serve , siap untuk dijalankan

Team 19
Terima kasih

