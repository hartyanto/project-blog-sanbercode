<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route Post


// Route::get('/post/pdf', function(){
//    $pdf = App::make('dompdf.wrapper');
//    $pdf->loadHTML('<h1>Test</h1>');
//    return $pdf->stream();
// });

/*----- Rout Export -----*/
Route::get('/post/exportpdf', 'PostController@exportPdf');
Route::get('/post/exportexcel', 'PostController@exportExcel');



Auth::routes();


/*----- Route Home -----*/
Route::get('/', 'HomeController@index')->name('home');
Route::get('/search', 'HomeController@search')->name('home.search');
Route::get('/teams', 'HomeController@teams')->name('home.teams');

/*----- Route Tags -----*/
Route::get('/tags', 'TagsController@index')->name('tags.index');
Route::get('/tags/{tags_id}/show', 'TagsController@show')->name('tags.show');

Route::middleware('auth')->group(function () {
  /*----- Route Post -----*/
  Route::get('/post', 'PostController@index')->name('post.index');
  Route::get('/post/create', 'PostController@create')->name('post.create');
  Route::post('/post', 'PostController@store')->name('post.store');
  Route::get('/post/{id}/edit', 'PostController@edit')->name('post.edit');
  Route::put('/post/{id}', 'PostController@update')->name('post.update');
  Route::delete('/post/{id}', 'PostController@destroy')->name('post.destroy');
  Route::get('post/{id}/likes', 'PostController@like')->name('post.like');
  Route::get('post/{id}/dislikes', 'PostController@dislike')->name('post.dislike');

  /*----- Route Users -----*/
  Route::get('/users/{user_id}/edit', 'UsersController@edit')->name('users.edit');
  Route::post('/users/{user_id}', 'UsersController@update')->name('users.update');
});

Route::get('/post/{id}', 'PostController@show')->name('post.show');

/*----- Route Comments -----*/
Route::post('/comments', 'CommentsController@store')->name('comments.store');
