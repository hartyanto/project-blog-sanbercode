<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_tag extends Model
{
    protected $fillable = ['post_id', 'tag_id'];
    protected $primaryKey = ['post_id', 'tag_id'];
    protected $table = 'post_tag';
    public $timestamps = false;
    public $incrementing = false;
}
