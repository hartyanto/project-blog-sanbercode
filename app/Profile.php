<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['full_name', 'phone', 'photo'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
