<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Tag;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tags = Tag::all();
        $posts = Post::latest()->simplePaginate(5);
        $title = 'Home';

        $top_posts = Post::withCount('comments')
            ->orderBy('comments_count', 'desc')->limit(2)
            ->get();
        return view('index', compact('posts', 'top_posts', 'tags', 'title'));
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $posts = Post::where('title', 'like', '%' . $keyword . '%')->simplePaginate(5);
        $tags = Tag::all();
        $title = 'Search';
        return view('index', compact('posts', 'tags', 'title'));
    }
//rubah disini
    public function teams()
    {
        return view('teams');
    }
}
