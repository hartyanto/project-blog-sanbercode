<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view('tags.index', compact('tags'));
    }

    public function show($id)
    {
        $tag = Tag::find($id);
        $tags = Tag::all();
        $posts = $tag->posts()->latest()->simplePaginate(5);
        $pesan = 'Belum ada post untuk tag ' . $tag->tag_name;
        return view('index', compact('posts', 'pesan', 'tags'));
    }
}
