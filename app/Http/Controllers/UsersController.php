<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null(Profile::where('user_id', $id)->first())) {
            $user = User::find($id);
            return view('users.edit', compact('user'));
        } else {
            $user = User::join('profiles', 'profiles.user_id', '=', 'users.id')
                ->where('users.id', $id)
                ->select('users.*', 'profiles.full_name', 'profiles.phone', 'profiles.photo')
                ->first();
            return view('users.edit', compact('user'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = $request->validate([
            'full_name' => 'required',
            'phone' => 'required|numeric',
        ]);

        auth()->user()->profile()->updateOrCreate(
            ['user_id' => $id],
            [
                'full_name' => $profile['full_name'],
                'phone' => $profile['phone'],
                'photo' => $request->photo
            ]
        );

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
