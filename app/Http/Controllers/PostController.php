<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use DB;
use App\Post;
use App\Comment;
use App\Tag;
use App\Post_tag;
use PDF;
use App\Exports\PostsExport;
use Maatwebsite\Excel\Facades\Excel;


class PostController extends Controller
{

	public function create()
	{
		$tags = Tag::all();
		$title = 'Create new post';
		return view('posts.create', compact('tags', 'title'));
	}
	//
	public function store(Request $request)
	{
		$request->validate([
			'title' => 'required|unique:posts',
			'body' => 'required',
			'tag_id' => 'required'
		]);

		if ($request->file('thumbnail')) {
			$thumbnail = $request->file('thumbnail')->store("img/posts");
			$post = auth()->user()->posts()->create([
				"title" => $request["title"],
				"thumbnail" => $thumbnail,
				"body" => $request["body"]
			]);
		} else {
			$post = auth()->user()->posts()->create([
				"title" => $request["title"],
				"body" => $request["body"]
			]);
		}
		$post->tags()->attach($request->tag_id);
		// Alert::success('Success', 'Berhasil Menambah Post');
		// return redirect(route('post.index'));
		return redirect(route('post.index'))->with('success', 'Post baru berhasil dibuat!');
	}

	public function index()
	{
		$posts = auth()->user()->posts;
		return view('posts.index', compact('posts'));
	}

	public function show($id)
	{
		$post = Post::find($id);
		$tags = Tag::all();
		$comments = Comment::where('post_id', $id)->get();
		$title = $post->title;
		return view('posts.show', compact('post', 'comments', 'tags', 'title'));
	}

	public function edit($id)
	{
		$post = Post::find($id);
		$tags = Tag::all();
		$tag_posts = Post::find($id)->tags;
		$title = 'Edit Post ' . $post->title;
		return view('posts.edit', compact('post', 'tags', 'tag_posts', 'title'));
	}

	public function update($id, Request $request)
	{
		$post = Post::find($id);
		$post->tags()->sync($request->tag_id);

		if ($request->thumbnail) {
			\Storage::delete($post->thumbnail);
			$thumbnail = $request->file('thumbnail')->store("img/posts");
		} else {
			$thumbnail = $post->thumbnail;
		}

		Post::where('id', $id)->update([
			'title' => $request['title'],
			'thumbnail' => $thumbnail,
			'body' => $request['body']
		]);

		return redirect(route('post.index'))->with('success', 'Berhasil update Post!');
	}

	public function destroy($id)
	{
		\Storage::delete(Post::find($id)->thumbnail);
		Post::where('id', $id)->delete();
		return redirect(route('post.index'))->with('success', 'Berhasil hapus Post!');
	}
	//

	public function exportPdf()
	{
		// $pdf = PDF::loadHTML('<h1>Hello World</h1>');
		$posts = Post::all();
		$pdf = PDF::loadView('posts.exportpdf', ['posts' => $posts]);
		return $pdf->download('post.pdf');
	}

	public function exportExcel()
	{
		return Excel::download(new PostsExport, 'posts.xlsx');
	}

	public function like($id)
	{
		auth()->user()->likes()->create([
			'post_id' => $id
		]);
		return back();
	}

	public function dislike($id)
	{
		auth()->user()->likes()->where('post_id', $id)->delete();
		return back();
	}
}
