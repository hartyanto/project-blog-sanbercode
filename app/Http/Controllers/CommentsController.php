<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Request $request)
    {

        $comment = $request->validate([
            'content' => 'required',
            'post_id' => 'required'
        ]);


        auth()->user()->comments()->create([
            'content' => $comment['content'],
            'post_id' => $comment['post_id']
        ]);

        return back();
    }
}
