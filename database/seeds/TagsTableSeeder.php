<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = collect(['Backpacker', 'Bali', 'Beach', 'Destination', 'Gear', 'Indonesia', 'Japan', 'Korea', 'Sunrise', 'Sunset', 'Temple', 'Tips', 'Yogyakarta']);
        $tags->each(function ($c) {
            \App\Tag::create([
                'tag_name' => $c
            ]);
        });
    }
}
