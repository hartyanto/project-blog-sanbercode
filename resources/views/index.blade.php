@extends('layouts.master')
@section('jumbotron')
    <div class="">
        <img src="{{ asset('img/jumbotron.jpg')}}"alt="Responsive image" style="width: 100%; height: 400px; object-fit: cover;">
    </div>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            @forelse ($posts as $post)    
            <div class="row">
                <div class="col-auto">
                    <div class="card mt-3 mr-0 shadow-sm">
                        <div class="card-header text-center">
                            <h5 class="text-uppercase">{{ $post->created_at->format('M') }}</h5>
                            <h4>{{ $post->created_at->format('j') }}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="card shadow-sm mt-3 ml-0">
                        @if (isset($post->thumbnail))
                        <img class="card-img-top" src="{{ asset('storage/'.$post->thumbnail)}}" alt="Card image cap" style="object-fit: cover; height: 200px; object-position: center">
                        @endif
                        <div class="card-body">
                            <h3 class="card-title">{{ $post->title }}</h3>
                            <div>
                                @foreach ( $post->tags as $tag )
                                <a href="{{ route('tags.show', $tag->id) }}" class="badge badge-primary">{{ $tag->tag_name}}</a>
                                @endforeach
                            </div>
                            <p class="card-text mt-3">{!! Str::limit($post->body, 300) !!}
                            <a href="{{ route('post.show', $post->id) }}">Read more</a> </p>
                            <div class="text-right d-flex justify-content-between">
                                <small class="align-self-end">
                                    Mempunyai {{ count($post->comments) }} komentar, {{ count($post->likes)}} orang menyukai ini.
                                </small>
                                <small>
                                    Ditulis {{ $post->author->name }}<br>
                                    Dipost pada {{ $post->created_at->diffForHumans() }}
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="row">
                <div class="col">
                    <div class="card shadow-sm mt-3">
                        <div class="card-body">
                            <div class="text-center font-italic">
                                <h4>{{ $pesan ?? 'Belum ada Post!' }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforelse
        </div>
        <div class="col-md-3">
            <div class="card mt-3 shadow-sm">
                <div class="card-header">
                    Search
                </div>
                <div class="card-body">
                    <form action="{{ route('home.search') }}" method="get">
                        <div class="input-group mb-3">
                            <input type="text" name="keyword" id="keyword" class="form-control" placeholder="search..." aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card mt-3 shadow-sm">
                <div class="card-header">
                    Tags
                </div>
                <div class="card-body">
                    @foreach ( $tags as $tag )
                        <a href="{{ route('tags.show', $tag->id) }}" class="badge badge-primary m-1">{{ $tag->tag_name}}</a>
                    @endforeach
                </div>
            </div>
            <div class="card mt-3 shadow-sm">
                <div class="card-header">
                    Follow Us
                </div>
                <div class="card-body">
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-instagram"></i></span><a href="">Laravelloka Official</a></div>
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-twitter"></i></span><a href="">Laravelloka Indonesia</a></div>
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-youtube"></i></span><a href="">Laravelloka</a></div>
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-facebook"></i></span><a href="">LaravellokaId</a></div>
                </div>
            </div>
            <div class="card mt-3 shadow-sm">
                <div class="card-header">
                    Top Posts
                </div>
                {{-- {{ dd($top_posts)}} --}}
                <div class="card-body">
                    @foreach ( $top_posts as $top_post )
                        <div class="card mt-2 p-2">
                            <a href="{{ route('post.show', $top_post->id) }}"><h5 class="card-title">{{ $top_post->title }}</h5></a>
                            <small>{{ $top_post->created_at->format('d F, Y') }}</small>
                            <p>{{ $top_post->comments_count }} komentar</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="div mt-3 d-flex justify-content-center">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
