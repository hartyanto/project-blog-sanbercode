@extends('adminlte.master')


@section('content')
<div class="mt-3 ml-3">
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Artikel</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                  {{ session('success')}}
                </div>
              @endif
              <div class="d-flex justify-content-between">
                <a class="btn btn-primary mb-2"  href="{{ route('post.create')}}">Buat Postingan Baru</a>
                <div>
                  <a class="btn btn-sm btn-danger"  href="/post/exportpdf">Export PDF</a>
                  <a class="btn btn-sm btn-success"  href="/post/exportexcel">Export Excel</a>
                </div>
              </div>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>

                  <tbody>
                  @forelse($posts as $key => $post)
                    <tr>
                      <td> {{$key+1}}</td>
                      <td> {{ $post->title }}</td>
                      <td> {{Str::limit($post->body, 200)}}</td>
                      <td class="d-flex justify-content-center align-center">
                        <a href="/post/{{$post->id}}" class="btn btn-info btn-sm">Show</a>
                        <a href="/post/{{$post->id}}/edit" class="btn btn-default btn-sm mx-2">Edit</a>
                        <form action="{{ route('post.destroy', $post->id) }}" method="post">
                          @csrf
                          @method('DELETE')
                          <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                      </td>
                    </tr> 
                    @empty
                    <tr>
                      <td colspan="4" class="text-center">Belum ada postingan </td>
                    </tr>
                    
                  @endforelse

                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
             
            </div>
</div>
@endsection