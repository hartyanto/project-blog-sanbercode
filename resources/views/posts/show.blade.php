@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            {{-- // --------- Post ---------- //  --}}
            <div class="row">
                <div class="col-auto">
                    <div class="card mt-3 mr-0 shadow-sm">
                        <div class="card-header text-center">
                            <h5 class="text-uppercase">{{ $post->created_at->format('M') }}</h5>
                            <h4>{{ $post->created_at->format('j') }}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="card shadow-sm mt-3 ml-0">
                        @if (isset($post->thumbnail))
                        <img class="card-img-top" src="{{ asset('storage/'.$post->thumbnail)}}" alt="Card image cap" style="object-fit: cover; height: auto; object-position: center">
                        @endif
                        <div class="card-body">
                            <h3 class="card-title">{{ $post->title }}</h3>
                            <div>
                                @foreach ( $post->tags as $tag )
                                <a href="{{ route('tags.show', $tag->id) }}" class="badge badge-primary">{{ $tag->tag_name}}</a>
                                @endforeach
                            </div>
                            <p class="card-text mt-3">{!! $post->body !!}
                            <div class="text-right d-flex justify-content-between">
                                <span class="align-self-end">
                                    @guest
                                        <span><i class="far fa-thumbs-up mr-2"></i><small>{{ count($post->likes)}} orang menyukai ini.</small></span>
                                    @else
                                        <a href="{{ count(auth()->user()->likes->where('post_id', $post->id)) == 1 ? route('post.dislike', $post->id) : route('post.like', $post->id)}}">
                                            <i class="{{ count(auth()->user()->likes->where('post_id', $post->id)) == 1 ? 'fas' : 'far' }} fa-thumbs-up mr-2"></i>
                                        </a>
                                        <small>{{ count($post->likes)}} orang menyukai ini.</small>
                                    @endguest
                                </span>
                                <small>
                                    Dipost pada {{ $post->created_at->diffForHumans() }}<br>
                                    <img style="width: 40px" src="{{ asset('img') }}/{{ $post->author->profile->photo ?? 'no-photo.png' }}" alt="" class="rounded mr-2">oleh {{ $post->author->name }}
                                </small>
                            </div>
                        </div>
                    </div>
                    {{-- // --------- Komentar ---------- // --}}
                    <div class="bg-light my-2 py-2">
                        <label for="content" class="ml-4 font-weight-bolder">Mempunyai {{ count($comments) }} komentar</label>
                    </div>
                    @forelse ($comments as $comment)
                    <div class="row">
                        <div class="col-auto">
                            <img style="width: 35px" src="{{ asset('img') }}/{{ $comment->author->profile->photo ?? 'no-photo.png' }}" alt="" class="rounded mt-2 ml-2">
                        </div>
                        <div class="col">
                            <div class="card mt-2">
                                <small class="mt-2 ml-2 font-italic">oleh <span class="font-weight-bolder">{{ $comment->author->name }}</span> pada {{ $comment->created_at }}</small>
                                <p class="ml-3 mt-2">{{ $comment->content }}</p>
                            </div>
                        </div>
                    </div>
                    @empty
                    <div class="row">
                        <div class="col">
                            <div class="card mt-2">
                                <div class="text-center py-5 font-italic">Belum Ada Komentar</div>
                            </div>
                        </div>
                    </div>
                    @endforelse
                    <div class="row">
                        <div class="col mt-2">
                            <form action="{{ route('comments.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <div class="bg-light my-2 py-2">
                                        <label for="content" class="ml-4 font-weight-bolder">Comment</label>
                                    </div>
                                    <textarea class="form-control" name="content" id="content" rows="8" style="resize: none"></textarea>
                                </div>
                                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                
                                @if (Auth::user())
                                    <button type="submit" class="btn btn-primary">Tambah Komentar</button>
                                @else
                                    <a href="{{ route('login') }}" class="btn-primary btn">Login</a>
                                @endif
                            </form>
                        </div>
                    </div>
                    {{-- // --------- Akhir Komentar ---------- // --}}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card mt-3 shadow-sm">
                <div class="card-header">
                    Search
                </div>
                <div class="card-body">
                    <form action="{{ route('home.search') }}" method="get">
                        <div class="input-group mb-3">
                            <input type="text" name="keyword" id="keyword" class="form-control" placeholder="search..." aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card mt-3 shadow-sm">
                <div class="card-header">
                    Tags
                </div>
                <div class="card-body">
                    @foreach ( $tags as $tag )
                        <a href="{{ route('tags.show', $tag->id) }}" class="badge badge-primary m-1">{{ $tag->tag_name}}</a>
                    @endforeach
                </div>
            </div>
            <div class="card mt-3 shadow-sm">
                <div class="card-header">
                    Follow Us
                </div>
                <div class="card-body">
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-instagram"></i></span><a href="">Laravelloka Official</a></div>
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-twitter"></i></span><a href="">Laravelloka Indonesia</a></div>
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-youtube"></i></span><a href="">Laravelloka</a></div>
                    <div class="mt-1"><span class="mr-2"><i class="fab fa-facebook"></i></span><a href="">LaravellokaId</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection