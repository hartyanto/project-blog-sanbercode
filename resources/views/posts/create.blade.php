@extends('adminlte.master')
@push('css')
  <link href="{{ asset('/dist/css/select2.min.css')}}" rel="stylesheet" />
@endpush

@section('content')
<div class="ml-3 mt-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Post</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('post.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" value="{{ old('title', '') }}" placeholder="Masukkan Judul">
          @error('tittle')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        
        <div class="form-group">
          <label for="thumbnail">Masukkan gambar:</label>
          <input type="file" name="thumbnail" id="thumbnail" class="form-control-file">
        </div>

        <div class="form-group">
          <label for="body">Isi</label>
          <textarea class="description" name="body" id="body" placeholder="Masukkan Isi Postingan">{{ old('body', '') }}</textarea>
          <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
          <script>
              tinymce.init({
                  selector:'textarea.description',
                  width: 900,
                  height: 300
              });
          </script>
          @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>

        <div class="control-group">
					<label for="tag_id">Tags :</label>
					<select id="tag_id" name="tag_id[]" multiple class="demo-default" style="width:50%" placeholder="Select tag">
            @foreach ($tags as $tag)
					    <option value="{{ $tag->id }}">{{ $tag->tag_name}}</option>
            @endforeach
          </select>
          @error('tag_id')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
				</div>
      </div>
      
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary" >Create</button>
        <a class="btn btn-warning" href="/post">Show</a>
      </div>
    </form>
  </div>
</div>
@endsection

@push('scripts')
	<script src="{{ asset('/dist/js/select2.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('#tag_id').select2();
		});
	</script>
@endpush