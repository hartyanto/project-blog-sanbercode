@extends('adminlte.master')
@push('css')
<link href="{{ asset('/dist/css/select2.min.css')}}" rel="stylesheet" />
@endpush

@section('content')
	<div class="card card-primary">
		<div class="card-header">
			<h3 class="card-title">Edit Question</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" action="{{ route('post.update', $post->id) }}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PUT')
		<div class="card-body">
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" value="{{old('title', $post->title)}}" placeholder="Masukan Judul">
				@error('judul')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="thumbnail">Masukkan gambar:</label>
				<input type="file" name="thumbnail" id="thumbnail" class="form-control-file">
			</div>
			<div class="form-group">
				<label for="body">Isi</label>
				<textarea name="body" id="body" class="description">{{old('body', $post->body)}}</textarea>
				<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
				<script>
					tinymce.init({
						selector:'textarea.description',
						width: 900,
						height: 300
					});
				</script>
				@error('isi')
					<div class="alert alert-danger">{{ $message }}</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="tag_id">Tags :</label>
					<select id="tag_id" name="tag_id[]" multiple="multiple" class="select2 form-control" style="width:50%">
						@foreach ($post->tags as $tag)
							<option value="{{ $tag->id }}" selected>{{ $tag->tag_name}}</option>
						@endforeach
						@foreach ( $tags as $tag)
							<option value="{{ $tag->id }}">{{ $tag->tag_name}}</option>
						@endforeach
					</select>
			</div>
		</div>
		<!-- /.card-body -->

		<div class="card-footer">
			<button type="submit" class="btn btn-primary">Edit</button>
			<a class="btn btn-warning" href="/post">Back</a>
		</div>
		</form>
	</div>
@endsection

@push('scripts')
	<script src="{{ asset('/dist/js/select2.min.js')}}"></script>
	<script>
		$(document).ready(function() {
			$('#tag_id').select2();
		});
	</script>
@endpush