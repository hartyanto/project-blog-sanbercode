@extends('adminlte.master')

@section('content')
    <div class="card mt-3 mx-3">
        <div class="row">
            <div class="col-auto">
                <div class="card body mt-3 ml-3">
                    <div>
                        <img src="{{ asset('img') }}/{{ $user->photo ?? 'no-photo.png' }}" style="width: 250px" alt="">
                    </div>
                </div>
            </div>
            <div class="col text-left">
                <div class="card-body">
                    <h5 class="text-right mt-3">{{ $user->name }}</h5>
                    <form action="{{ route('users.update', Auth::id()) }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="full_name">Nama lengkap</label>
                            <input type="text" value="{{ $user->full_name ?? '' }}" name="full_name" id="full_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="phone">Nomor Telepon</label>
                            <input type="text" value="{{ $user->phone ?? '' }}" name="phone" id="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="photo">Foto</label>
                            <input type="text" value="{{ $user->photo ?? '' }}" name="photo" id="photo" class="form-control">
                        </div>
        
                        <button type="submit" class="btn btn-primary">Ubah Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection