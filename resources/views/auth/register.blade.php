<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 d-none d-md-block image-container"></div>
			<div class="col-lg-6 col-md-6 form-container">
                <div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
					<div class="logo-text mt-5 mb-3">
						<a class="" href="{{ route('home') }}"><h3 class="font-weight-bolder">Laravelloka</h3></a>
					</div>
					<div class="heading mb-3">
						<h4>Create an account</h4>
					</div>
					<form method="POST" action="{{ route('register') }}">
						@csrf
						<div class="form-input">
							<span><i class="fa fa-user"></i></span>
							<input type="text" name="name" id="name" placeholder="Name" value="{{ old('name') }}" required>
							@error('name')
							<div class="text-danger" role="alert">
								<small>{{ $message }}</small>
							</div>
							@enderror
						</div>
						<div class="form-input">
							<span><i class="fa fa-envelope"></i></span>
							<input type="email" name="email" id="email" placeholder="Email Address" value="{{ old('email') }}" required>
							@error('email')
							<div class="text-danger" role="alert">
								<small>{{ $message }}</small>
							</div>
							@enderror
						</div>
						<div class="form-input">
							<span><i class="fa fa-lock"></i></span>
							<input type="password" name="password" id="password" placeholder="Password" required>
							@error('password')
							<div class="text-danger" role="alert">
								<small>{{ $message }}</small>
							</div>
							@enderror
						</div>
						<div class="form-input">
							<span><i class="fa fa-lock"></i></span>
							<input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirm Password" required>
						</div>
						<div class="text-left mb-3">
							<button type="submit" class="btn">Register</button>
						</div>
						<div class="text-white mb-3">or register with</div>
						<div class="row mb-3">
							<div class="col-4">
								<a href="" class="btn btn-block btn-social btn-facebook">
									<i class="fa fa-facebook"></i>
								</a>
							</div>
							<div class="col-4">
								<a href="" class="btn btn-block btn-social btn-google">
									<i class="fa fa-google"></i>
								</a>
							</div>
							<div class="col-4">
								<a href="" class="btn btn-block btn-social btn-twitter">
									<i class="fa fa-twitter"></i>
								</a>
							</div>
						</div>
						<div class="text-white">Already have an account?
							<a href="{{ route('login')}}" class="login-link">Login here</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>