@extends('layouts.master')


@section('content')
<h3 class="mt-4 mb-4">Anggota Tim 19</h3>
  <div class="row">
    <div class="col-md-4">
      <!-- Widget: user widget style 1 -->
      <div class="card card-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-info">
          <h3 class="d-flex align-items-center justify-content-center" style="height: 100px;">Riyani Jana Yanti</h3>
        </div>
        <div class="">
          <img class="" src="{{ asset('adminlte/dist/img/avatar2.png')}}" alt="User Avatar" style="object-fit: cover; object-position: center; width: 100%;">
        </div>
        <div class="card-footer">
          <div class="row">
            <!-- /.col -->
            <div class="border-right">
              <div class="description-block">
                <h5 class="description-header">Quotes</h5>
                <span class="description-text"> "We will never know the real answer before we try."</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.widget-user -->
    </div>
    <!-- /.col -->
    <div class="col-md-4">
      <!-- Widget: user widget style 1 -->
      <div class="card card-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="bg-info">
          <h3 class="text-center d-flex  align-items-center" style="height: 100px;">Praga Hartyanto Prabowo</h3>
        </div>
        <div class="">
          <img class="img-circle elevation-2" src="{{ asset('adminlte/dist/img/avatar5.png')}}" alt="User Avatar" style="object-fit: cover; object-position: center; width: 100%;">
        </div>
        <div class="card-footer">
          <div class="row">
            <!-- /.col -->
            <div class="border-right">
              <div class="description-block">
                <h5 class="description-header">Quotes</h5>
                <span class="description-text">"if you cannot do great thinks, do mall things in a great way. "</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.widget-user -->
    </div>
    <!-- /.col -->
    <div class="col-md-4">
      <!-- Widget: user widget style 1 -->
      <div class="card card-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-info">
          <h3 class="d-flex align-items-center justify-content-center" style="height: 100px;">Arip Rahman Hakim</h3>
        </div>
        <div class="">
          <img class="img-circle elevation-2" src="{{ asset('adminlte/dist/img/avatar5.png')}}" alt="User Avatar" style="object-fit: cover; object-position: center; width: 100%;">
        </div>
        <div class="card-footer">
          <div class="row">
            <!-- /.col -->
            <div class="border-right">
              <div class="description-block">
                <h5 class="description-header">Quotes</h5>
                <span class="description-text">"Life is a journey to be experienced, not a problem to be solved." </span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.widget-user -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
@endsection