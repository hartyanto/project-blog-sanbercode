@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="list-group mt-3">
                @forelse ($tags as $tag)    
                    <a href="{{ route('tags.show', $tag->id) }}" class="list-group-item list-group-item-action">
                        {{ $tag->tag_name }}
                    </a>
                @empty
                    <a href="#" class="list-group-item list-group-item-action disabled">
                        {{ 'Belum ada tag!' }}
                    </a>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection
